﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab_2.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Lab_2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        List<ListViewItem> items;

        public MainPage()
        {
            InitializeComponent();
            InitList();

        }

        void Handle_Refreshing(object sender, EventArgs e)
        {
            exampleListView.IsRefreshing = false;
        }

        void InitList()
        {
            items = new List<ListViewItem>();

            items.Add(new ListViewItem
            {
                Name = "Male Peacock",
                Text = "Female Peacock"
                
            });

            items.Add(new ListViewItem
            {
                Name = "Male Elephant Seal",
                Text = "Female Elephant Seal"
            });

            items.Add(new ListViewItem
            {
                Name = "Male Pheasant",
                Text = "Female Pheasant"
            });

            exampleListView.ItemsSource = items;
        }
    }
}